# Trade Analysis Dashboard API Design

## Endpoints

### TEMPLATE: «Human-readable of the endpoint»

- Endpoint path: «path to use»
- Endpoint method: «HTTP method»
- Query parameters:

  - «name»: «purpose»

- Headers:

  - Authorization: Bearer token

- Request shape (JSON):

  ```json
  «JSON-looking thing that has the
  keys and types in it»
  ```

- Response: «Human-readable description
  of response»
- Response shape (JSON):

  ```json
  «JSON-looking thing that has the
  keys and types in it»
  ```
