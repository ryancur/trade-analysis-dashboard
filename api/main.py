from fastapi import FastAPI
from routers import initial_route


app = FastAPI()
app.include_router(initial_route.router)
