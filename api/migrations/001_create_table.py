"""
This is the first migrations file. This is a template and will need to be changed
when ready to build actual tables.
"""

steps = [
    [
        # create the table
        """
        CREATE TABLE test (
            id SERIAL PRIMARY KEY NOT NULL,
            name VARCHAR(1000) NOT NULL,
            from_date DATE NOT NULL,
            to_date DATE NOT NULL,
            thoughts TEXT
        );
        """,
        # drop the table
        """
        DROP TABLE test;
        """,
    ]
]
